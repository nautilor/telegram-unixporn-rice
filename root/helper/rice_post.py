#!/usr/bin/env python3

""" Functions to help work with riced stored in the database """

from mongoengine.errors import DoesNotExist
from telegram import User, CallbackQuery, message
from telegram_utils.utils.tutils import create_button
from root.model.rice_post import RicePost
import telegram_utils.utils.logger as logger


def save_post(original_post: int, message_id: int, user: User):
    """ Store the rice in the database """
    logger.info(f"saving rice {message_id} from {user.id}")
    return RicePost(
        original_post=original_post,
        message_id=message_id,
        user_id=user.id,
        first_name=user.first_name,
        username=user.username,
        upvotes=[],
        downvotes=[],
    ).save()


def update_post(rice_post: RicePost):
    """ Update a rice """
    logger.info(f"updating rice {rice_post.code}")
    rice_post.save()


def find_by_original_post(original_post: int):
    try:
        logger.info(f"searching rice {original_post}")
        return RicePost.objects.get(original_post=original_post)
    except DoesNotExist:
        return None


def find_by_code(code: str):
    """ Retrieve a rice from his code """
    logger.info(f"searching rice {code}")
    try:
        return RicePost.objects.get(code=code)
    except DoesNotExist:
        return None


def find_by_message_id(message_id: int):
    """ Retrieve a rice from his message_id """
    try:
        logger.info(f"searching rice {message_id}")
        return RicePost.objects.get(message_id=message_id)
    except DoesNotExist:
        return None


def build_rice_keyboard(rice: RicePost):
    """ Create the keyboard used to upvote/downvote """
    upvotes = len(rice.upvotes)
    downvotes = len(rice.downvotes)
    return [
        [
            create_button(f"❤️  {upvotes}", f"rice.upvote_{rice.code}"),
            create_button(f"💔  {downvotes}", f"rice.downvote_{rice.code}"),
        ]
    ]


def get_callback_code(callback: CallbackQuery):
    """ Retreive the rice code from the callback """
    return callback.data.split("_")[-1]
