#!/usr/bin/env python3

""" File to describe a rice stored in the database """

from uuid import uuid4
from mongoengine import ListField, IntField, StringField
from root.model.base_model import BaseModel


def generate():
    """ Generate a unique code to identify the rice """
    return uuid4().hex[:17]


class RicePost(BaseModel):
    """ Class to describe a rice stored in the database """

    code = StringField(unique=True, default=generate)
    original_post = IntField(required=True)
    message_id = IntField(required=True)
    user_id = IntField(required=True)
    username = StringField()
    first_name = StringField()
    upvotes = ListField(IntField())
    downvotes = ListField(IntField())
