#!/usr/bin/env python3

""" File to manage the start of the bot """

from telegram.ext import (
    Updater,
    MessageHandler,
    Filters,
    CallbackQueryHandler,
    CommandHandler,
)
from telegram_utils.utils.tutils import notice_admin
from telegram_utils.utils.misc import environment
from root.manager.rice import parse_rice, upvote_rice, downvote_rice
from root.manager.start import handle_start

TOKEN: str = environment("TOKEN")


def connect():
    """ Create a Bot instances and listen for incoming updates """
    updater: Updater = Updater(TOKEN, use_context=True)
    add_handlers(updater)
    notice_admin("Bot started...")
    updater.start_polling(clean=True)


def add_handlers(updater: Updater):
    """ Add all the handlers for the update """
    updater.dispatcher.add_handler(CommandHandler("start", handle_start))
    updater.dispatcher.add_handler(MessageHandler(Filters.document.image, parse_rice))
    updater.dispatcher.add_handler(MessageHandler(Filters.photo, parse_rice))
    updater.dispatcher.add_handler(MessageHandler(Filters.regex(r"\#rice"), parse_rice))
    updater.dispatcher.add_handler(
        CallbackQueryHandler(pattern="rice.upvote_.*", callback=upvote_rice)
    )
    updater.dispatcher.add_handler(
        CallbackQueryHandler(pattern="rice.downvote_.*", callback=downvote_rice)
    )
