#!/usr/bin/env python3

""" Manage the rice received from Telegram updates """

from telegram import Update, Message
from telegram.ext import CallbackContext
from telegram.error import BadRequest
import telegram_utils.utils.logger as logger
from telegram_utils.utils.tutils import (
    delete_if_private,
    send_and_delete,
    not_allowed_in_private,
)
from root.constants.message import START_MESSAGE, ENGLISH_GROUP, retrive_message


def is_private(message: Message) -> bool:
    """ Return if a message has been send over a private chat """
    if message.chat.type == "private":
        delete_if_private(message)
        return True
    return False


def handle_start(update: Update, context: CallbackContext):
    """ Send a miniguide on how to work with the bot >D """
    message: Message = update.effective_message
    if is_private(message):
        send_and_delete(
            message.chat.id, retrive_message(START_MESSAGE, ENGLISH_GROUP), timeout=15
        )
