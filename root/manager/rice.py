#!/usr/bin/env python3

""" Manage the rice received from Telegram updates """

import re
from types import LambdaType
from urllib import request
from telegram import Update, Message, PhotoSize, InlineKeyboardMarkup, User, File
from telegram.ext import CallbackContext
from telegram.error import BadRequest
import telegram_utils.utils.logger as logger
from telegram_utils.utils.tutils import (
    delete_if_private,
    not_allowed_in_private,
    send_and_delete,
)
from root.helper.rice_post import (
    save_post,
    build_rice_keyboard,
    update_post,
    find_by_code,
    get_callback_code,
    find_by_original_post,
)
from root.constants.message import (
    ALLOWED_CHATS,
    CHAT_NOT_ALLOWED,
    ENGLISH_GROUP,
    MISSING_PHOTO,
    MESSAGE_TOO_OLD,
    MISSING_DESCRIPTION,
    PRIVATE_CHAT,
    MALFORMED_DESCRIPTION,
    retrive_message,
)
from root.constants.environment import RICE_FIELD
from root.model.rice_post import RicePost
from root.constants.miscellanous import RICE_CAPTION_REGEX


def is_private(message: Message) -> bool:
    """ Return if a message has been send over a private chat """
    if message.chat.type == "private":
        delete_if_private(message)
        not_allowed_in_private(message)
        return True
    return False


def parse_rice(update: Update, context: CallbackContext):
    """ Parse a new rice """
    logger.info("Ready to parse a new rice")
    message: Message = update.effective_message
    reply_message: Message = message.reply_to_message
    language = str(message.chat.id)

    ## ignore not allowed chats ##
    if not str(message.chat.id) in ALLOWED_CHATS:
        send_and_delete(message.chat.id, CHAT_NOT_ALLOWED, timeout=10)
        return

    ## ignore if the message has been already posted ##
    if find_by_original_post(message.message_id):
        logger.warn(f"message {message.message_id} already present")
        return

    ## Ignore any post coming from private messages ##
    if is_private(message):
        logger.warn("Received #rice in a private chat")
        send_and_delete(
            message.chat.id, retrive_message(PRIVATE_CHAT, ENGLISH_GROUP), timeout=15
        )
        return

    caption: str = message.caption if message.caption else message.text
    ## Ignore messages without the hashtag ##
    if not "#rice" in caption:
        return
    ## If the caption is empty send error ##
    caption = re.sub("#rice", "", caption)
    if not caption:
        send_and_delete(
            message.chat.id, retrive_message(MISSING_DESCRIPTION, language), timeout=10
        )
        return

    ## Check if the description if formatted correctly ##
    caption = re.findall(RICE_CAPTION_REGEX, caption)
    if not caption:
        send_and_delete(
            message.chat.id,
            retrive_message(MALFORMED_DESCRIPTION, language),
            timeout=10,
        )
        return

    caption = caption[0]

    ## Check if the description if formatted correctly ##
    if caption[-1] == "]":
        send_and_delete(
            message.chat.id,
            retrive_message(MALFORMED_DESCRIPTION, language),
            timeout=10,
        )
        return

    user: User = reply_message.from_user if reply_message else message.from_user
    photo: list = reply_message.photo if reply_message else message.photo

    if photo:
        # TODO: handle multiple pictures
        logger.info(f"Received an album of {len(photo)}")
        photo: PhotoSize = photo[0]

    if not photo:
        if message.document:
            photo: str = message.document.get_file().file_path
            photo: bytes = request.urlopen(photo)
        elif reply_message:
            if reply_message.document:
                photo: str = reply_message.document.get_file().file_path
                photo: bytes = request.urlopen(photo)

    # If there's not photo tell the user and stop
    if not photo:
        logger.warn(
            f"The message {message.message_id} from {user.username} has no photo"
        )
        send_and_delete(
            message.chat.id, retrive_message(MISSING_PHOTO, language), timeout=15
        )
        return

    """ When a rice is stored in the database a random key gets generated to identify the rice
        Since the code is used to generate the Telegram keyboard these are steps involved to make this work
            - Save the rice to generate the unique code
            - Send the message to the channel to retrieve the message_id
            - Update the rice with the message_id """
    ## Save the rice with a temporary message_id ##
    logger.info("Storing rice to database")
    rice: RicePost = save_post(message.message_id, message.message_id, user)

    logger.info("Sending rice to the rice field")
    ## Send the rice to the channel and retrieve the message_id ##
    message: Message = context.bot.send_photo(
        chat_id=RICE_FIELD,
        photo=photo,
        parse_mode="HTML",
        reply_markup=InlineKeyboardMarkup(build_rice_keyboard(rice)),
    )
    ##  Update it with the new message_id on the database ##
    rice.message_id = message.message_id
    update_post(rice)


def get_rice_by_callback(callback):
    """ retrieve the rice from the callback code """
    rice_code = get_callback_code(callback)
    return find_by_code(rice_code)


def edit_rice(update: Update, context: CallbackContext, rice: RicePost):
    """ Edit the message with the updated rice """
    message: Message = update.effective_message
    language = str(message.chat.id)
    try:
        context.bot.edit_message_caption(
            chat_id=RICE_FIELD,
            message_id=rice.message_id,
            reply_markup=InlineKeyboardMarkup(build_rice_keyboard(rice)),
        )
        # If everything went well just update the rice in the database
        update_post(rice)
    except BadRequest as e:
        logger.error("error while updating the message [{e.message}]")
        context.bot.answer_callback_query(
            update.callback_query.id,
            text=retrive_message(MESSAGE_TOO_OLD, language),
            show_alert=True,
        )


def downvote_rice(update: Update, context: CallbackContext):
    """ Downvote an existing rice """
    context.bot.answer_callback_query(update.callback_query.id)
    user_id: int = update.effective_user.id
    rice = get_rice_by_callback(update.callback_query)
    if not rice:
        logger.warn(f"rice '{update.callback_query.data}' not in the database")
        return

    upvotes: list = rice.upvotes
    downvotes: list = rice.downvotes

    # Add the user to the downvote list if not present
    if not user_id in downvotes:
        downvotes.append(user_id)
        if user_id in upvotes:
            # Remove the user from the list to prevent clicking like and dislike
            upvotes.remove(user_id)
    else:
        # Remove the user from the list to prevent double click
        downvotes.remove(user_id)
    rice.upvotes = upvotes
    rice.downvotes = downvotes

    # Show an alert if some error happened during the update of the message
    edit_rice(update, context, rice)


def upvote_rice(update: Update, context: CallbackContext):
    """ Upvote an existing rice """
    context.bot.answer_callback_query(update.callback_query.id)
    user_id: int = update.effective_user.id
    rice = get_rice_by_callback(update.callback_query)
    if not rice:
        logger.warn(f"rice '{update.callback_query.data}' not in the database")
        return

    upvotes: list = rice.upvotes
    downvotes: list = rice.downvotes

    # Add the user to the upvote list if not present
    if not user_id in upvotes:
        upvotes.append(user_id)
        if user_id in downvotes:
            # Remove the user from the list to prevent clicking like and dislike
            downvotes.remove(user_id)
    else:
        # Remove the user from the list to prevent double click
        upvotes.remove(user_id)
    rice.upvotes = upvotes
    rice.downvotes = downvotes

    # Show an alert if some error happened during the update of the message
    edit_rice(update, context, rice)
