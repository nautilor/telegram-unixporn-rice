#!/usr/bin/env python3

""" Various messages used across the project """

import telegram_utils.utils.logger as logger

SPANISH_GROUP = "-1001180390261"
RUSSIAN_GROUP = "-1001184092739"
ENGLISH_GROUP = "-1001189167476"
TEST____GROUP = "-1001382571795"

GROUP_LINK = "https://t.me/joinchat/RuFBdDX81BYzPGA4"
NETWORK_GROUP_LINK = "https://t.me/r_unixporn_group"

ALLOWED_CHATS = [SPANISH_GROUP, RUSSIAN_GROUP, ENGLISH_GROUP, TEST____GROUP]

CHAT_NOT_ALLOWED = (
    "❌  This chat is not allowed to use this bot.\n\nIf you wanna use this bot please join any"
    f' <a href="{NETWORK_GROUP_LINK}">group</a> of our <a href="{NETWORK_GROUP_LINK}">network</a> to post your rice.'
    )

MISSING_PHOTO = "MISSING_PHOTO"
MESSAGE_TOO_OLD = "MESSAGE_TOO_OLD"
MISSING_DESCRIPTION = "MISSING_DESCRIPTION"
MALFORMED_DESCRIPTION = "MALFORMED_DESCRIPTION"
PRIVATE_CHAT = "PRIVATE_CHAT"
START_MESSAGE = "START_MESSAGE"

MESSAGES = {
    ENGLISH_GROUP: {
        "MISSING_PHOTO": (
            "This function requires you to quote a photo or to attach one in the message."
        ),
        "MESSAGE_TOO_OLD": (
            "Unable to perform the action, maybe the message is too old?"
        ),
        "MISSING_DESCRIPTION": (
            "Please provide a description for your rice.\n\nAn example of description is <code>[i3] Darcula Theme</code>"
        ),
        "MALFORMED_DESCRIPTION": (
            "The description is not formatted correctly.\n\nAn example of description is <code>[i3] Darcula Theme</code>"
        ),
        "PRIVATE_CHAT": (
            "This type of operation cannot be performed from a private chat.\n\n"
            f'If you want to post your rice please join our group <a href="{GROUP_LINK}">HERE</a> and try sending the request again ☺️'
        ),
        "START_MESSAGE": (
            "Hello and Welcome 👋, this is the Rice Manager used in the r/unixporn Telegram community"
            f" and it will allow users to share their rice from any group to the rice field.\n\n"
            f'If you want to share your rice please make sure to join our group <a href="{GROUP_LINK}">HERE</a>'
        ),
    },
    RUSSIAN_GROUP: {
        "MISSING_PHOTO": (
            "Вы должны или процитировать картинку, или прикрепить одну в сообщении."
        ),
        "MESSAGE_TOO_OLD": (
            "Невозможно выполнить действие. Может быть сообщение слишком старое?"
        ),
        "MISSING_DESCRIPTION": (
            "Пожалуйста добавьте описание к райсу.\n\nВот пример описания: <code>[i3] Darcula Theme</code>"
        ),
        "MALFORMED_DESCRIPTION": (
            "Описание оформленно неправильно.\n\nВот пример описания: <code>[i3] Darcula Theme</code>"
        ),
    },
    SPANISH_GROUP: {
        "MISSING_PHOTO": (
            "Esta función requiere que cites o adjuntes una imagen en el mensaje"
        ),
        "MESSAGE_TOO_OLD": (
            "No se pudo realizar la acción, tal vez el mensaje es demasiado antiguo"
        ),
        "MISSING_DESCRIPTION": (
            "Por favor añade una descripción sobre tu rice.\n\nPor ejemplo: [i3] Darcula Theme"
        ),
        "MALFORMED_DESCRIPTION": (
            "El formato de la descripción es incorrecto.\n\nEjemplo de una descripción válida: [i3] Darcula Theme"
        ),
    },
    TEST____GROUP: {
        "MISSING_PHOTO": (
            "This function requires you to quote a photo or to attach one in the message."
        ),
        "MESSAGE_TOO_OLD": (
            "Unable to perform the action, maybe the message is too old?"
        ),
        "MISSING_DESCRIPTION": (
            "Please provide a description for your rice.\n\nAn example of description is <code>[i3] Darcula Theme</code>"
        ),
        "MALFORMED_DESCRIPTION": (
            "The description is not formatted correctly.\n\nAn example of description is <code>[i3] Darcula Theme</code>"
        ),
    },
}


def retrive_message(code: str, language: str):
    try:
        return MESSAGES[language][code]
    except Exception:
        logger.error(f"Unable to find message for {code} in {language}")
        return MESSAGES[ENGLISH_GROUP][code]
