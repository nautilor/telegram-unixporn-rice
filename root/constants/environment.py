#!/usr/bin/env python3

""" Some environment variables used across the project """

from telegram_utils.utils.misc import environment

RICE_FIELD: str = environment("RICE_FIELD")
