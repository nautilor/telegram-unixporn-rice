#!/usr/bin/env python3

""" The main program starting everything """

from telegram_utils.utils.database import db_connect
from root.manager.bot import connect

"""
                ████████              
            ████        ████          
          ██            ░░  ██        
        ██░░            ░░    ██      
      ██                        ██    
    ██                ░░          ██     ████████████████████████████████████
    ██    ░░                      ██     ██ RICE BOT FOR TELEGRAM UNIXPORN ██
  ██              ░░                ██   ████████████████████████████████████
  ██          ████████████          ██   ██   Developer: Edoardo Zerbo   ██
  ██░░        ████████████    ░░    ██   ██████████████████████████████████
  ██  ░░      ████████████      ░░░░██   ██ Telegram: t.me/WMD_Edoardo ██
  ██░░        ████████████      ░░░░██   ████████████████████████████████
  ██░░░░░░░░░░████████████░░░░░░░░░░██
    ██░░░░░░░░████████████░░░░░░░░██  
      ████████████████████████████ 

  ████████████████████████████████████████████████████████████████████████████
  ██ Description: This bot i used by the unixporn community of Telegram.    ██
  ██              Within any of the community group you can send a photo    ██
  ██              of your rice with the hashtag '#rice' and it will         ██
  ██              automatically send it over the rice channel where you can ██
  ██              upvote or downvote the rice;                              ██
  ████████████████████████████████████████████████████████████████████████████
"""

if __name__ == "__main__":
    db_connect()
    connect()
